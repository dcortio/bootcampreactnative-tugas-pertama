console.log('==========')
console.log('1. Animal Class ')
console.log('Release 0')

class Animal {
    constructor(name) {
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }
    get amal() {
        return this.name
    }
    set amal(x) {
        this.name = x
    }

}
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log('Release 1')

class Ape extends Animal{

    constructor(name, amount) {
      super(name);
      this.legs = amount 
    }
    yell() {
      console.log ('auoo');
    }
  }
  
  class Frog extends Animal {
    constructor(name) {
      super(name);
      
    }
    jump() {
      console.log('hop hop');
    }
  }
  
  var sungokong = new Ape('kera sakti', 2);
  sungokong.yell()
  console.log(sungokong.name);
  console.log(sungokong.legs);
  console.log(sungokong.cold_blooded);
  
  var kodok = new Frog('buduk');
  kodok.jump()
  console.log(kodok.name)
  console.log(kodok.legs)
  console.log(kodok.cold_blooded)

console.log('==========')
console.log('2. Function to Class ')

class Clock {
    constructor({ template }){
        this.template = template;
    }
    render (){
        var date = new Date();
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs)

        console.log(output)
    }
 
    stop(){
      clearInterval(this.timer);
    }


start(){

        this.render();
        this.timer = setInterval(()=>this.render(), 1000);
    }

}
var clock = new Clock({template: 'h:m:s'});
clock.start();