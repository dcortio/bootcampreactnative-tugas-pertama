console.log('==========')
console.log ('1. Mengubah fungsi menjadi fungsi arrow')

const golden = function goldenFunction(){
    console.log("this is golden!!")
  }
   
golden()

var golden2 = goldenFunction2 = () => {
    console.log('this is golden!!')
    }
golden2()

console.log('==========')
console.log('2. Sederhanakan menjadi Object literal di ES6')

const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
   
  //Driver Code 
newFunction("William", "Imoh").fullName() 

const newFunction2 = function literal(firstName, lastName){
    return {
      fullName: function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
const {firstName, lastName} = newFunction2
newFunction2('william', 'imoh').fullName()

console.log('==========')
console.log('3. Destructuring')

const newObject = {
    firstName2: "Harry",
    lastName2: "Potter Holt",
    destination2: "Hogwarts React Conf",
    occupation2: "Deve-wizard Avocado",
    spell2: "Vimulus Renderus!!!"
  }
const {firstName2, lastName2, destination2, occupation2, spell2} = newObject
// Driver code
console.log(firstName2, lastName2, destination2, occupation2, spell2)

console.log('==========')
console.log('4. Array Spreading')

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

//Driver Code
console.log(combined)

console.log('==========')
console.log('5. Template Literals')

const planet = "earth"
const view = "glass"
var before = `Lorem ` + view + `dolor sit amet, ` +  
    `consectetur adipiscing elit,` + planet + `do eiusmod tempor ` +
    `incididunt ut labore et dolore magna aliqua. Ut enim` +
    ` ad minim veniam`
 
// Driver Code
console.log(before) 