console.log('LOOPING PERTAMA')
var flag = 0;
while(flag < 20) {
    flag++;
    flag++;
    console.log(flag + ' - I love coding')
}

console.log('LOOPING KEDUA')
var flag = 22
while(flag > 2) {
    flag--;
    flag--;
    console.log(flag + ' - I love coding')
}

console.log(' ')

for (var x = 1; x < 21; x++) {
    if(x % 2 == 0){
        console.log(x + ' - ' + 'Berkualitas')
    }else if(x % 3 == 0) {
        console.log(x + ' - ' + 'I Love Coding')
    }else if(x % 2 !== 0) {
        console.log(x + ' - ' + 'Santai')
    }
}

console.log(' ')

var Persegi = 1
while(Persegi < 5) {
    Persegi++;
    console.log('########')
}

console.log(' ')

var hasil = ''; {
    for (var i = 1; i < 8; i++) {
        for (var j = 1; j <= i; j++) {
            hasil += '#';
        }
        hasil += '\n';
    }
    console.log(hasil);
}

console.log(' ')

var hasil = ''; {
    for (var i = 1; i < 9; i++) {
        if(i % 2 == 0) {
            console.log(' # # # #')
        }else{
            console.log('# # # # ')
        }
        
        hasil += '\n';
    }
    console.log(hasil);
}

console.log(' ')